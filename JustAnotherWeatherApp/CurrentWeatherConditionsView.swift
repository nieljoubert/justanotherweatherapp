//
//  CurrentWeatherConditionsView.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/02.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import UIKit

@IBDesignable
class CurrentWeatherConditionsView: UIView
{
	@IBOutlet var dateLabel: UILabel!
	@IBOutlet var timeLabel: UILabel!	
	@IBOutlet var imageView: UIImageView!
	@IBOutlet var summary: UILabel!
	@IBOutlet var tempLabel: UILabel!
	@IBOutlet var noDataLabel: UILabel!
	
	required override init(frame: CGRect) {
		super.init(frame:frame)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func initWithCurrentData(current:Currently)
	{
		dateLabel.text = Utils.dateStringFromUnixtime(current.time!, dateDisplayType:DateDisplayType.DateHeaderFormat)
		timeLabel.text = "Last refresh @ \(Utils.dateStringFromUnixtime(current.time!, dateDisplayType:DateDisplayType.ShortTime))"
		summary.text = current.summary
		tempLabel.text = "\(Utils.convertFtoC(current.temperature!))°";
		noDataLabel.hidden = true
	}
	

}
