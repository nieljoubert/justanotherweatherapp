//
//  DailyCollectionViewCell.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/04.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import UIKit

//@IBDesignable
class DailyCollectionViewCell: UICollectionViewCell {

	@IBOutlet var imageView: UIImageView!
	@IBOutlet var dayLabel: UILabel!
	@IBOutlet var summaryLabel: UILabel!

	@IBOutlet var tempLabel: UILabel!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

	func initCellWithDayData(data:DailyData)
	{
		dayLabel.text = Utils.dateStringFromUnixtime(data.time!, dateDisplayType:DateDisplayType.ShortDay)
		summaryLabel.text = data.summary
		tempLabel.text = "\(Utils.convertFtoC(data.temperatureMin!))° - \(Utils.convertFtoC(data.temperatureMax!))°"
	}
}
