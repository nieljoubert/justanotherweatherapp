//
//  Currently.h
//
//  Created by Niel Joubert on 2015/07/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CurrentlyObjc : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double temperature;
@property (nonatomic, assign) double windSpeed;
@property (nonatomic, assign) double humidity;
@property (nonatomic, assign) double windBearing;
@property (nonatomic, strong) NSString *precipType;
@property (nonatomic, assign) double cloudCover;
@property (nonatomic, assign) double time;
@property (nonatomic, assign) double dewPoint;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, assign) double precipIntensity;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) double ozone;
@property (nonatomic, assign) double apparentTemperature;
@property (nonatomic, assign) double pressure;
@property (nonatomic, assign) double precipProbability;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
