//
//  WeatherResult.h
//
//  Created by Niel Joubert on 2015/07/25
//  Copyright (c) 2015 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HourlyObjc, DailyObjc, CurrentlyObjc, FlagsObjc;

@interface WeatherResult : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double offset;
@property (nonatomic, strong) HourlyObjc *hourly;
@property (nonatomic, assign) double longitude;
@property (nonatomic, strong) DailyObjc *daily;
@property (nonatomic, strong) NSString *timezone;
@property (nonatomic, strong) CurrentlyObjc *currently;
@property (nonatomic, assign) double latitude;
@property (nonatomic, strong) FlagsObjc *flags;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
