//
//  HourlyTableViewCell.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/02.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import UIKit

//@IBDesignable
class HourlyTableViewCell: UITableViewCell
{

	@IBOutlet var timeLabel: UILabel!
	@IBOutlet var iconImageView: UIImageView!
	@IBOutlet var statusLabel: UILabel!
	@IBOutlet var tempLabel: UILabel!
	@IBOutlet var feelsTempLabel: UILabel!
	
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}
	
	func initWithHourlyData(data: HourlyData)
	{
		timeLabel.text = Utils.dateStringFromUnixtime(data.time!, dateDisplayType:DateDisplayType.ShortTime)
		statusLabel.text = data.summary
		tempLabel.text = "\(Utils.convertFtoC(data.temperature!))°"
		feelsTempLabel.text = "feels like \(Utils.convertFtoC(data.apparentTemperature!))°"
	}
    
}
