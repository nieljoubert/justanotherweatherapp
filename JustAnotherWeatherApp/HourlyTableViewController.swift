//
//  HourlyTableViewController.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/02.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import UIKit

class HourlyTableViewController: BaseTableViewController
{
	var days : [String]?
	var hours : [HourlyData]?
//	var tableData: [(day: String, hour: [HourlyData])] = []
	var tableData: [(sec: Int, rows: Int)] = []
	
	var hourlyInfo : Hourly? {
		
		didSet {
			
//			tableData = [(String, [HourlyData])] = []
//			tableData : [(Int, Int)] = []
//			days = [String]()
//			hours = [HourlyData]()
//			
//			var oldDate: Int = 0
//			
//			if let hourData = self.hourlyInfo?.data
//			{
//				for hour in hourData
//				{
//					if oldDate != hour.time!
//					{
//						hours?.append(hour)
//						days?.append(Utils.dateStringFromUnixtime(hour.time!, dateDisplayType: DateDisplayType.DateHeaderFormat))
//						
//						oldDate = hour.time!
//						
//						var dayString: String = Utils.dateStringFromUnixtime(hour.time!, dateDisplayType: DateDisplayType.DateHeaderFormat)
//
////						tableData += [(sec: days?.count, rows: hours?.count)]
//						tableData.append(sec: days!.count, rows: hours!.count)
//						println("T - \(tableData)")
//						hours = [HourlyData]()
//					}
//					else
//					{
//						hours?.append(hour)
//					}
//					
//					println("d - \(days)")
//					println("h - \(hours)")
//					println("----------------------------------------------")
//					
//				}
//			}
		}
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
	
		self.tableView.registerNib(UINib(nibName: "HourlyTableViewCell", bundle: nil), forCellReuseIdentifier: "HourlyTableViewCell")
	}
	
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int
	{
		return 1;
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		if let hourData = self.hourlyInfo?.data
		{
			return hourData.count;
		}
		return 0;
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCellWithIdentifier("HourlyTableViewCell", forIndexPath: indexPath) as! HourlyTableViewCell
		
		let row = indexPath.row
		
		var data: HourlyData
		
		if let hourData = self.hourlyInfo?.data
		{
			data = hourData[row] as HourlyData
			cell.initWithHourlyData(data)
		}
		
		return cell;
	}

	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
	{
		return 70;
	}
	
	
	
	
	
	
	
}
