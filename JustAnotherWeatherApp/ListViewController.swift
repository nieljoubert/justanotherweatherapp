//
//  ListViewController
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/07/24.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import UIKit
import CoreLocation

class ListViewController: BaseViewController, CLLocationManagerDelegate, WeatherManagerDelegate,
	UITableViewDataSource, UITableViewDelegate,
	UICollectionViewDelegate, UICollectionViewDataSource
{
	var locationManager: CLLocationManager!
	var weatherData: Weather?
	
	@IBOutlet var headerView: CurrentWeatherConditionsView!
	@IBOutlet var tableView: UITableView!
	@IBOutlet var collectionView: UICollectionView!
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		locationManager = CLLocationManager()
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestWhenInUseAuthorization()
		loadViewFromNib()
		
//		collectionView.registerClass(DailyCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "DailyCollectionViewCell")
		tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "UITableViewCell")
		collectionView.registerNib(UINib(nibName: "DailyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DailyCollectionViewCell")
	}
	
	func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
	{
		let coord = locationManager.location.coordinate
		println("locations = \(coord.latitude) \(coord.longitude)")
		locationManager.stopUpdatingLocation()
		
		WeatherManager.sharedManager.getDataFromLocation(locationManager.location.coordinate)
		WeatherManager.sharedManager.delegate = self;
		
	}

	func loadViewFromNib()
	{
		let bundle = NSBundle(forClass: self.dynamicType)
		let nib = UINib(nibName: "CurrentWeatherConditionsView", bundle: bundle)
		let hView = nib.instantiateWithOwner(self, options: nil)[0] as! CurrentWeatherConditionsView
	
		headerView = hView;
		tableView.addSubview(headerView)
	}
	
	func gotWeatherData(weatherData: Weather)
	{
		self.weatherData = weatherData
		
		if let current = weatherData.currently
		{
			headerView.initWithCurrentData(current)
		}
		tableView.reloadData()
		collectionView.reloadData()
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
	{
		if segue.identifier == "HourlySegue"
		{
			let destinationVC = segue.destinationViewController as! HourlyTableViewController
		
			if let hourlyInfo = self.weatherData?.hourly
			{
				destinationVC.hourlyInfo = hourlyInfo
			}
		}
        else if segue.identifier == "LoadshedSegue"
        {
            
        }
	}
	
	@IBAction func getCurrentLocation(sender: UIBarButtonItem)
	{
		locationManager.startUpdatingLocation();
//        LoadsheddingManager.sharedManager.getLoadshedStatus()
//        LoadsheddingManager.sharedManager.delegate = self;
	}
	
	// MARK: TableView
	func numberOfSectionsInTableView(tableView: UITableView) -> Int
	{
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		return 2
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		var cell = tableView.dequeueReusableCellWithIdentifier("TextCell") as? UITableViewCell
		
		if cell == nil
		{
			cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "UITableViewCell")
		}
		
		if indexPath.row == 0
		{
			cell!.textLabel?.text = "Hourly"
		}
		
		if indexPath.row == 1
		{
			cell!.textLabel?.text = "LoadShedding"
		}
		cell!.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
		
		return cell!
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		if indexPath.row == 0
		{
			performSegueWithIdentifier("HourlySegue", sender: self)
		}
        else
        {
            performSegueWithIdentifier("LoadshedSegue", sender: self)
        }
		
        tableView.deselectRowAtIndexPath(indexPath, animated: true)	
	}
	
	// MARK: CollectionView
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if let days = self.weatherData?.daily?.data
		{
			return days.count
		}
		
		return 0
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier("DailyCollectionViewCell", forIndexPath: indexPath) as! DailyCollectionViewCell

		var info: DailyData
		
		if let dailyData = self.weatherData?.daily?.data
		{
			info = dailyData[indexPath.row] as DailyData
			cell.initCellWithDayData(info)
		}
		
		return cell
		
	}
	
	
}

