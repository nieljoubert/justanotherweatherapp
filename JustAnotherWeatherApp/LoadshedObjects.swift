//
//  LoadshedObject.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/05.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import Foundation
import ObjectMapper

struct LoadshedStatus: Mappable {
    
    var stage: Int? = 0
    var timestamp: NSDate?
    
    static func newInstance() -> Mappable {
        return LoadshedStatus()
    }
    
    mutating func mapping(map: Map) {
        stage       <- map["active_stage"]
        timestamp   <- (map["timestamp"],DateTransform())
    }
}

struct LoadshedArea: Mappable {
    
    var areaID: Int? = 0
    var name: String?
    var description: String?
    
    static func newInstance() -> Mappable {
        return LoadshedArea()
    }
    
    mutating func mapping(map: Map) {
        areaID      <- map["area_id"]
        name        <- map["name"]
        description <- map["description"]
    }
}

struct LoadshedSchedule: Mappable {
    
    var areaID: Int? = 0
    var stage: Int? = 0
    var date: String?
    var dayOfMonth: Int? = 0
	var outages: [String]?
    
    static func newInstance() -> Mappable {
        return LoadshedSchedule()
    }
    
    mutating func mapping(map: Map) {
        areaID      <- map["area_id"]
        stage       <- map["stage"]
        date        <- map["date"]
        dayOfMonth  <- map["day_of_month"]
		outages		<- map["outages"]
    }
}


struct LoadshedNextOutage: Mappable {
    
    var areaID: Int? = 0
    var stage: Int? = 0
    var date: String?
    var period: String?
    
    static func newInstance() -> Mappable {
        return LoadshedNextOutage()
    }
    
    mutating func mapping(map: Map) {
        areaID      <- map["area_id"]
        stage       <- map["stage"]
        date        <- map["next_outage"]
        period      <- map["next_outage_period"]
    }
}







