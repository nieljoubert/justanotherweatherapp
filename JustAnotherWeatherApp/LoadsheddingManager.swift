//
//  LoadsheddingManager.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/05.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import Foundation
import Alamofire
import SwiftSpinner

protocol LoadsheddingManagerDelegate
{
    func gotLoadSheddingStatus(statusData:LoadshedStatus)
    func gotLoadSheddingAreas(areas:[LoadshedArea])
	func gotLoadSheddingShedule(schedule:LoadshedSchedule)
	func gotLoadSheddingNextOutage(outage:LoadshedNextOutage)
    func gotLoadsheddingFullSchedule(fullShedule:[(stage:Int,outages:[String])])
}

class LoadsheddingManager
{
    static let sharedManager = LoadsheddingManager()
    var delegate: LoadsheddingManagerDelegate! = nil
    
    var currentStatus: LoadshedStatus?
    var areas: [LoadshedArea]?
	var schedule: LoadshedSchedule?
	var nextOutage: LoadshedNextOutage?
    var fullShedule: [(stage:Int,outages:[String])]?
	
    init()
    {
        
    }
	
 /**
	Retrieve the last known status of Eskom's load shedding.
	
	The resulting object contains 2 fields: active_stage and timestamp.
	
	active_stage - integer 1, 2, 3 or 4 or null indicating that no load shedding is taking place.
	timestamp - the ISO8601 (UTC) timestamp of when that status was recorded.
	
	curl "http://whereismypower.co.za/api/get_status"
	
	{"active_stage": 2, "timestamp": "2015-02-03T23:01:55.201Z"}
	
	*/
	
    func getLoadshedStatus()
    {
//        SwiftSpinner.show("Getting Loadshedding Status")
		
        Alamofire.request(.GET, Constants.LOADSHEDDING_API_STATUS, parameters: nil)
            .responseObject { (response: LoadshedStatus?, error: NSError?) in

                self.currentStatus = response
                
                if let response = response, delegate = self.delegate
                {
                    delegate.gotLoadSheddingStatus(response)
                    SwiftSpinner.hide()
                }
        }
    }
	
	
	/**
	Retrieve the list of loadshedding areas in Cape Town.
	
	The resulting object is an array of 16 areas. Each area contains 3 fields: area_id, name and description.
	
	area_id - area number. From 1 to 16.
	name  - short area name such as 'Area 1', 'Area 5' etc.
	description -  a line describing the suburbs included in this area.
	
	"http://whereismypower.co.za/api/list_areas"
	
	[{"area_id": 1, "name": "Area 1", "description": "Bellville"},{"area_id": 2, "name": "Area 2", "description": "Maitland, Milnerton"}, ... , {"area_id": 16, "name": "Area 16", "description": "Retreat, Philippi"}]
	*/
	
	func getLoadshedAreas()
	{
		SwiftSpinner.show("Getting Areas")
		
		Alamofire.request(.GET, Constants.LOADSHEDDING_API_AREAS, parameters: nil)
			.responseArray { (response: [LoadshedArea]?, error: NSError?) -> Void in
				
				self.areas = response
				
				if let response = response, delegate = self.delegate
				{
					delegate.gotLoadSheddingAreas(response)
					SwiftSpinner.hide()
				}
		}
	}
	
	/**
	This request expects exactly 3 parameters: area, date and stage
	
	area - is the area number. From 1 to 16.
	date - is the date. Preferably in YEAR-MONTH-DAY format. 'today' and 'tomorrow' can also be used.
	stage - is the loadshedding stage. Preferably a number 1, 2, 3 or 4. 3A is equivalent to 3. 3B is equivalent to 4.

	The resulting object contains 5 fields: outages, area_id, stage, date and day_of_month.
	
	outages - is an array containing the outages. 
				The array contains a number of time ranges of the form '0X:00 - 0Y:30'. 
				This can be reliably split on ', ' and ' - ' if data needs to be extracted.
	area_id - confirmation of the area number requested.
	stage - confirmation of the stage requested.
	date -  confirmation of the date requested.
	day_of_month - raw day of the month value.
	
	http://whereismypower.co.za/api/get_schedule?area=11&date=2014-05-05&stage=3B
	{"outages": ["02:00 - 04:30", "10:00 - 12:30", "18:00 - 20:30"], "area_id": 11, "stage": 4,"date": "2014-05-05", "day_of_month": 5}
	
	*/
	func getLoadshedOutages(area:Int, date:NSDate, stage:Int, completion:(Int,[String]) -> Void)
    {
		var dateV: String = "today"//NSDate()
//		let params: [String : AnyObject?] = ["area": area, "stage": stage, "date": date]
		let urlString = Constants.LOADSHEDDING_API_SCHEDULE + "?area=\(area)&date=\(dateV)&stage=\(stage)"
		
		Alamofire.request(.GET, urlString, parameters: nil)
			.responseObject { (response: LoadshedSchedule?, error: NSError?) -> Void in
				
				self.schedule = response
				
				if let response = response, delegate = self.delegate
				{
//					delegate.gotLoadSheddingShedule(response)
                    
                    completion((response.stage!, response.outages!))
				}
		}
	}
    
    func getCompleteSchedule(area:Int, date:NSDate)
    {
        SwiftSpinner.show("Getting schedule")
        
        self.fullShedule = []
        
        getLoadshedOutages(area, date: date, stage:1) {
            (result: (stage:Int,outages:[String])) in
            
            self.fullShedule?.append(result)
            
            self.getLoadshedOutages(area, date: date, stage:2) {
                (result: (stage:Int,outages:[String])) in
                
                self.fullShedule?.append(result)
                
                self.getLoadshedOutages(area, date: date, stage:3) {
                    (result: (stage:Int,outages:[String])) in
                    
                    self.fullShedule?.append(result)
                    
                    self.getLoadshedOutages(area, date: date, stage:4) {
                        (result: (stage:Int,outages:[String])) in
                        
                        self.fullShedule?.append(result)
                        
                        self.delegate.gotLoadsheddingFullSchedule(self.fullShedule!)
                        SwiftSpinner.hide()
                    }
                }
            }
        }
    }
	
 /**
	Retrieve the next time that loadshedding will occur in the givent area under stome stage.
	
	This request expects exactly 2 parameters: area and stage
	
	area -  is the area number. From 1 to 16.
	stage -  is the loadshedding stage. Preferably a number 1, 2, 3 or 4. 3A is equivalent to 3. 3B is equivalent to 4.
	
	The resulting object contains 4 fields: area_id, stage, next_outage and next_outage_period.
	
	next_outage - ISO8601 formatted datetime indicating the start of the loadshedding period that will next influence the area and stage.
					If the area is currently loadshedding, this field will show the start of the current period.
	next_outage_period - string showing the loadshedding period as '0X:00 - 0Y:30'.
	area_id - confirmation of the area number requested.
	stage - confirmation of the stage requested.

	http://whereismypower.co.za/api/get_next_loadshedding?area=11&stage=3B
	{"area_id": 11, "stage": 4, "next_outage": "2015-04-19T12:00:00+02:00", "next_outage_period": "12:00 - 14:30"}
	
	*/
	
	func getLoadshedNextOutage()
	{
		SwiftSpinner.show("Getting next outage")
		
//		var params =
		
		
		Alamofire.request(.GET, Constants.LOADSHEDDING_API_NEXT, parameters: nil)
			.responseObject { (response: LoadshedNextOutage?, error: NSError?) in
				
				self.nextOutage = response
				
				if let response = response, delegate = self.delegate
				{
					delegate.gotLoadSheddingNextOutage(response)
					SwiftSpinner.hide()
				}
		}
	}
}