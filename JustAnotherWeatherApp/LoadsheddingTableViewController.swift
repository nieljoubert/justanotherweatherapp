//
//  LoadsheddingTableViewController.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/06.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import UIKit
import CoreLocation

class LoadsheddingTableViewController: BaseTableViewController, LoadsheddingManagerDelegate
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "UITableViewCell")
		LoadsheddingManager.sharedManager.delegate = self
		LoadsheddingManager.sharedManager.getLoadshedStatus()
//		LoadsheddingManager.sharedManager.getLoadshedAreas()
//		LoadsheddingManager.sharedManager.getLoadshedOutages(2, date: NSDate(), stage: 4)
	}
	
    func gotLoadSheddingStatus(statusData: LoadshedStatus)
    {
        if let stage = statusData.stage
        {
            if stage > 0
            {
                title = "Currently Stage: \(LoadsheddingManager.sharedManager.currentStatus?.stage!)"
            }
            
            title =  "Currently No Loadshedding"
        }
        
        LoadsheddingManager.sharedManager.getCompleteSchedule(2, date: NSDate())
    }
	
	func gotLoadSheddingAreas(areas: [LoadshedArea])
	{
		var area:LoadshedArea?
		
		for area in areas
		{
//			println("Areas: \(area.areaID)")
		}
	}
	
	func gotLoadSheddingShedule(schedule: LoadshedSchedule)
	{
		if let outages = schedule.outages
		{
			for outage in outages
			{
//				println("Outages: \(outage)")
			}
		}
	}

	func gotLoadSheddingNextOutage(outage: LoadshedNextOutage)
	{
		if let period = outage.period
		{
//			println("Next: \(period)")
		}
	}
    
    func gotLoadsheddingFullSchedule(fullShedule:[(stage:Int,outages:[String])])
    {
        println("FS: \(fullShedule)")
        
        tableView.reloadData()
    }
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!)
	{
//		if segue.identifier == "HourlySegue"
//		{
//			let destinationVC = segue.destinationViewController as! HourlyTableViewController
//			
//			if let hourlyInfo = self.weatherData?.hourly
//			{
//				destinationVC.hourlyInfo = hourlyInfo
//			}
//		}
	}
	
	
	// MARK: - TableView
	override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if let stages = LoadsheddingManager.sharedManager.fullShedule?.count
        {
            return stages
        }
        
        return 0
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
        if let stageInfo = LoadsheddingManager.sharedManager.fullShedule?[section] as (stage:Int,outages:[String])?
        {
            return stageInfo.outages.count
        }
        
        return 0;
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
		var cell = tableView.dequeueReusableCellWithIdentifier("TextCell") as? UITableViewCell
		
		if cell == nil
		{
			cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "UITableViewCell")
		}
        
        if let stageInfo = LoadsheddingManager.sharedManager.fullShedule?[indexPath.section] as (stage:Int,outages:[String])?
        {
            if let outage = stageInfo.outages[indexPath.row] as String?
            {
                cell!.textLabel?.text = outage
            }
        }
        
		cell!.accessoryType = UITableViewCellAccessoryType.None
		
		return cell!
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
//		if indexPath.row == 0
//		{
//			performSegueWithIdentifier("HourlySegue", sender: self)
//		}
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)		
	}
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if let stageInfo = LoadsheddingManager.sharedManager.fullShedule?[section] as (stage:Int,outages:[String])?
        {
            return "Stage \(stageInfo.stage)"
        }
        
        return ""
    }
}
