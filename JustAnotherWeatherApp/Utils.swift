//
//  Utils.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/08/02.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import Foundation
import UIKit

enum DateDisplayType
{
	case ShortTime
	case DateTime
	case DateHeaderFormat
	case ShortDay
}

struct Constants {
	static var API_KEY = "2ada20769d00963b356c39ffd223d2ba"
	static var CURRENT_WEATHER = "https://api.forecast.io/forecast/" + API_KEY + "/%f,%f"
	static var LOADSHEDDING_API = "http://whereismypower.co.za/api/"
	static var LOADSHEDDING_API_STATUS = LOADSHEDDING_API + "get_status/"
	static var LOADSHEDDING_API_AREAS = LOADSHEDDING_API + "list_areas/"
	static var LOADSHEDDING_API_SCHEDULE = LOADSHEDDING_API + "get_schedule/"
	static var LOADSHEDDING_API_NEXT = LOADSHEDDING_API + "get_next_loadshedding/"
}

class Utils
{
	
	class func convertFtoC(fahrenheit : Double) -> Int
	{
		return Int(round((fahrenheit - 32) / 1.8))
	}
	
	class func dateStringFromUnixtime(unixTime: Int, dateDisplayType: DateDisplayType) -> String
	{
		let timeInSeconds = NSTimeInterval(unixTime)
		let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
		
		let dateFormatter = NSDateFormatter()
		
		switch (dateDisplayType)
		{
		case .ShortTime:
			dateFormatter.dateFormat = "HH:mm";
			break
			
		case .DateTime:
			dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
			break
			
		case .DateHeaderFormat:
			dateFormatter.dateFormat = "eee, dd MMMM YYYY";
			break
			
		case .ShortDay:
			dateFormatter.dateFormat = "eee";
			break
		}
		
		return dateFormatter.stringFromDate(weatherDate)
	}
	
	class func weatherIconFromString(stringIcon: String) -> UIImage
	{
		var imageName: String
		
		switch stringIcon {
		case "clear-day":
			imageName = "clear-day"
		case "clear-night":
			imageName = "clear-night"
		case "rain":
			imageName = "rain"
		case "snow":
			imageName = "snow"
		case "sleet":
			imageName = "sleet"
		case "wind":
			imageName = "wind"
		case "fog":
			imageName = "fog"
		case "cloudy":
			imageName = "cloudy"
		case "partly-cloudy-day":
			imageName = "partly-cloudy"
		case "partly-cloudy-night":
			imageName = "cloudy-night"
		default:
			imageName = "default"
		}
		
		var iconImage = UIImage(named: imageName)
		return iconImage!
	}
}