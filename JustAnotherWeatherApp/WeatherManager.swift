//
//  WeatherManager.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/07/24.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import Foundation
import Alamofire
import ResponseDetective
import AlamofireObjectMapper
import ObjectMapper
import CoreLocation
import SwiftSpinner

protocol WeatherManagerDelegate
{
    func gotWeatherData(weatherData:Weather)
}

class WeatherManager
{
    static let sharedManager = WeatherManager()
    var delegate: WeatherManagerDelegate! = nil
    
    var currentWeather: Weather?
    
    init()
	{
		// init some (likely) global state
	}
	
	func getDataFromLocation(coordinates: CLLocationCoordinate2D)
	{
//		// request
//		InterceptingProtocol.registerRequestInterceptor(JSONInterceptor())
//		InterceptingProtocol.registerResponseInterceptor(JSONInterceptor())
//		
//		let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
//		configuration.protocolClasses = [InterceptingProtocol.self]
//		
//		let manager = Alamofire.Manager(configuration: configuration)
		
		let urlString = String(format: Constants.CURRENT_WEATHER, coordinates.latitude, coordinates.longitude)
	 
		SwiftSpinner.show("Getting weather data")
		
		Alamofire.request(.GET, urlString, parameters: nil)
			.responseObject { (response: Weather?, error: NSError?) in

				self.currentWeather = response
			
				if let response = response
				{
					if let delegate = self.delegate
					{
						delegate.gotWeatherData(response)
						SwiftSpinner.hide()
					}
				}
		}
	}
}
