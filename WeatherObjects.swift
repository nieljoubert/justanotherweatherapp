//
//  WeatherObject.swift
//  JustAnotherWeatherApp
//
//  Created by Niel Joubert on 2015/07/25.
//  Copyright (c) 2015 Niel Joubert. All rights reserved.
//

import Foundation
import ObjectMapper

class Weather: Mappable {
	
	var offset: Int? = 0
	var hourly: Hourly?
	var longitude: Double? = 0
	var latitude: Double? = 0
	var daily: Daily?
	var timezone: Int? = 0
	var currently: Currently?
	var flags: Flags?
	
	class func newInstance() -> Mappable {
		return Weather()
	}
	
	func mapping(map: Map) {
		offset		<- map["offset"]
		hourly		<- map["hourly"]
		longitude	<- map["longitude"]
		latitude	<- map["latitude"]
		daily		<- map["daily"]
		timezone	<- map["timezone"]
		currently	<- map["currently"]
		flags		<- map["flags"]
		//		birthday    <- (map["birthday"], DateTransform())
	}
}

struct Hourly: Mappable {
	
	var summary: String?
	var icon: String?
	var data: [HourlyData]?
	
	static func newInstance() -> Mappable {
		return Hourly()
	}
	
	mutating func mapping(map: Map) {
		summary	<- map["summary"]
		icon	<- map["icon"]
		data	<- map["data"]
	}
}

struct HourlyData: Mappable {
	
	var temperature: Double? = 0
	var apparentTemperature: Double? = 0
	var windSpeed: Double? = 0
	var humidity: Double? = 0
	var windBearing: Int? = 0
	var precipType: String?
	var precipProbability: Double? = 0
	var precipIntensity: Double? = 0
	var cloudCover: Double? = 0
	var time: Int? = 0
	var dewPoint: Double? = 0
	var summary: String?
	var icon: String?
	var ozone: Double? = 0
	var pressure: Double? = 0
	
	static func newInstance() -> Mappable {
		return HourlyData()
	}
	
	mutating func mapping(map: Map) {
		temperature			<- map["temperature"]
		apparentTemperature	<- map["apparentTemperature"]
		windSpeed			<- map["windSpeed"]
		humidity			<- map["humidity"]
		windBearing			<- map["windBearing"]
		precipType			<- map["precipType"]
		precipProbability	<- map["precipProbability"]
		precipIntensity		<- map["precipIntensity"]
		cloudCover			<- map["cloudCover"]
		time				<- map["time"]
		dewPoint			<- map["dewPoint"]
		summary				<- map["summary"]
		icon				<- map["icon"]
		ozone				<- map["ozone"]
		pressure			<- map["pressure"]
	}
}

struct Daily: Mappable {
	
	var summary: String?
	var icon: String?
	var data: [DailyData]?
	
	static func newInstance() -> Mappable {
		return Daily()
	}
	
	mutating func mapping(map: Map) {
		summary	<- map["summary"]
		icon	<- map["icon"]
		data	<- map["data"]
	}
}

struct DailyData: Mappable {
	
	var windSpeed: Double? = 0
	var humidity: Double? = 0
	var windBearing: Int? = 0
	var precipType: String?
	var precipProbability: Double? = 0
	var precipIntensity: Double? = 0
	var cloudCover: Double? = 0
	var time: Int? = 0
	var dewPoint: Double? = 0
	var summary: String?
	var icon: String?
	var ozone: Double? = 0
	var pressure: Double? = 0
	
	var precipIntensityMax: Double? = 0
	var precipIntensityMaxTime: Int? = 0
	
	var sunriseTime: Int? = 0
	var sunsetTime: Int? = 0
	var moonPhase: Double? = 0

	var temperatureMin: Double? = 0
	var temperatureMax: Double? = 0
	var temperatureMinTime: Int? = 0
	var temperatureMaxTime: Int? = 0
	
	var apparentTemperatureMin: Double? = 0
	var apparentTemperatureMax: Double? = 0
	var apparentTemperatureMinTime: Int? = 0
	var apparentTemperatureMaxTime: Int? = 0
	
	static func newInstance() -> Mappable {
		return DailyData()
	}
	
	mutating func mapping(map: Map) {
		windSpeed					<- map["windSpeed"]
		humidity					<- map["humidity"]
		windBearing					<- map["windBearing"]
		precipType					<- map["precipType"]
		precipProbability			<- map["precipProbability"]
		precipIntensity				<- map["precipIntensity"]
		cloudCover					<- map["cloudCover"]
		time						<- map["time"]
		dewPoint					<- map["dewPoint"]
		summary						<- map["summary"]
		icon						<- map["icon"]
		ozone						<- map["ozone"]
		pressure					<- map["pressure"]
		
		precipIntensityMax			<- map["precipIntensityMax"]
		precipIntensityMaxTime		<- map["precipIntensityMaxTime"]
		sunriseTime					<- map["sunriseTime"]
		sunsetTime					<- map["sunsetTime"]
		moonPhase					<- map["moonPhase"]
		temperatureMin				<- map["temperatureMin"]
		temperatureMax				<- map["temperatureMax"]
		temperatureMinTime			<- map["temperatureMinTime"]
		temperatureMaxTime			<- map["temperatureMaxTime"]
		apparentTemperatureMin		<- map["apparentTemperatureMin"]
		apparentTemperatureMax		<- map["apparentTemperatureMax"]
		apparentTemperatureMinTime	<- map["apparentTemperatureMinTime"]
		apparentTemperatureMaxTime	<- map["apparentTemperatureMaxTime"]
	}
}

struct Currently: Mappable {
	
	var temperature: Double? = 0
	var windSpeed: Double? = 0
	var humidity: Double? = 0
	var windBearing: Int? = 0
	var precipType: String?
	var cloudCover: Double? = 0
	var time: Int? = 0
	var dewPoint: Double? = 0
	var summary: String?
	var icon: String?
	var precipIntensity: Double? = 0
	var ozone: Double? = 0
	var apparentTemperature: Double? = 0
	var pressure: Double? = 0
	var precipProbability: Double? = 0
	
	static func newInstance() -> Mappable {
		return Currently()
	}
	
	mutating func mapping(map: Map) {
		windSpeed					<- map["windSpeed"]
		humidity					<- map["humidity"]
		windBearing					<- map["windBearing"]
		precipType					<- map["precipType"]
		precipProbability			<- map["precipProbability"]
		precipIntensity				<- map["precipIntensity"]
		cloudCover					<- map["cloudCover"]
		time						<- map["time"]
		dewPoint					<- map["dewPoint"]
		summary						<- map["summary"]
		icon						<- map["icon"]
		ozone						<- map["ozone"]
		pressure					<- map["pressure"]
		temperature					<- map["temperature"]
		apparentTemperature			<- map["apparentTemperature"]
	}
}

struct Flags: Mappable {
	
	var madisStations: [String]?
	var units: String?
	var isdStations: [String]?
	var sources: [String]?
	
	static func newInstance() -> Mappable {
		return Flags()
	}
	
	mutating func mapping(map: Map) {
		madisStations	<- map["madis-stations"]
		units			<- map["units"]
		isdStations		<- map["isd-stations"]
		sources			<- map["units"]
	}

}